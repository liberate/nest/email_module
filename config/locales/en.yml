en:
  ##
  ## FILTERS
  ##

  filter_name: "Filter Name"
  filter_conditions: "Conditions"
  match: "Match"
  destination_folder: "Destination Folder"
  folder_description: |
    The folder name is case sensitive. To delete the message, send to the
    "Trash" folder.
  add_new_filter: "Add New Filter"
  from: "From"
  to: "To"
  cc: "CC"
  subject: "Subject"
  body: "Body"
  list_id: "List-Id"
  all_of: "All Of"
  any_of: "Any Of"
  contains: "Contains"
  starts_with: "Starts With"
  ends_with: "Ends With"
  is_exactly: "Is Exactly"
  mail_filters_description: |
    Mail filters allow you to sort your mail into different folders as it
    arrives. The filters configured here are applied by the mail server
    itself and will work with all mail clients. The filters are applied in
    the order they are listed.
  up: "Up"
  down: "Down"

  ##
  ## MAIL
  ##

  mail_settings: "Mail Settings"
  no_mailbox: "No Mailbox"
  address: "Address"
  forward: "Forwarding address"
  forward_info: "If specified, all mail will be forwarded to this address."
  forward_error_alias: "cannot be your own alias"
  forward_error_exist: "must point to an existing address"
  quota: "Quota"
  megabytes: "Megabytes"
  spam: "Spam"
  filters: "Filters"
  general: "General"
  vacation: "Vacation"
  vacation_responder: "Vacation Automatic Response"
  vacation_description: |-
    When turned on, an automated reply is sent for all incoming messages. The automated reply is sent at
    most once every other day to each recipient.
  subject: "Subject"
  message: "Message"

  ##
  ## ALIASES
  ##

  aliases: "Aliases"
  send: "Send"
  receive: "Receive"
  aliases_info: |-
    Aliases allow you to **send** and **receive** mail using alternate email addresses.
    Only addresses listed here are allowed to appear in the **From** field of messages you send.
  confirmation_pending: "Confirmation Pending"
  resend_confirmation_email: "Resend Confirmation Email"
  check_email_for_confirmation: |-
    The address %{alias} must be confirmed before it can be used.
    Check your inbox for a confirmation email.
  confirmation_email_sent: "Confirmation email sent to %{alias}"
  confirm_new_alias: "Confirm New Alias"
  confirm_alias_success: "The alias %{alias} is now confirmed and ready to be used."
  new_alias: "New Alias"
  alias_limit: "Alias limit reached"
  alias_error: "Alias %{alias} is invalid"
  alias_error_length_short: "Alias %{alias} is too short"
  alias_error_length_long: "Alias %{alias} is too long"
  alias_error_taken: "Alias %{alias} is already taken"
  alias_error_email: "Alias %{alias} is not a valid email address"
  alias_error_domain: "The email provider %{domain} requires that you use their system for all aliases"
  alias_error_missing: "Alias address is required"
  alias_error_forbidden: "You do not have permission to create aliases on domain %{domain}"
  alias_confirmation_email_body: |-
    The account %{address} has requested permission to send mail using the alias %{recipient}.

    To grant this permission, open the link %{link} in a web browser.

    If you do not wish to allow this, you can ignore this email.

  ##
  ## ENCRYPTED STORAGE
  ##

  enabled: "Enabled"
  disabled: "Disabled"
  please_enable_storage_key: |
    Your account is not currently using personally encrypted storage.
  enable_storage_key: "Enable Encrypted Storage"
  storage_key_enabled: "Encrypted Storage is now enabled."
  storage_key_heading: "Encrypted Storage"
  storage_key_details: |
    When Encrypted Storage is enabled, your email stored on the server is only
    readable while you are logged in.

    However, if you lose your password and your recovery code, your stored
    messages will be totally unreadable, even by you.

    This form of encryption is not end-to-end: messages are encrypted only when
    at rest on the server. For end-to-end encryption, you must use OpenPGP
    in your mail client.

  ##
  ## SPAM SETTINGS
  ##

  spam_settings: "Spam Settings"
  spam_sensitivity: "Spam sensitivity"
  less_sensitive: "Less"
  more_sensitive: "More"
  spam_score: |
    If you make the spam filter more sensitive, it will catch more spam but
    also incorrectly catch more legitimate email.
  block_list: "Block list"
  block_list_info: |
    This is a list of email addresses that you want to block. Mail from these
    addresses will almost always be classified as spam. Use "*@domain.org"
    if you want to block an entire domain.
  block_list_invalid_email: "Block list contains an invalid email address."
  allow_list: "Allow list"
  allow_list_info: |
    This is a list of email addresses that you always want to receive mail
    from. Mail from these addresses will never be classified as spam.
    Use "*@domain.org" if you want to allow an entire domain.
  allow_list_invalid_email: "Allow list contains an invalid email address."
  spam_lang: |
    If you choose one or more languages, the filter will attempt to identify
    the language of the incoming email and only allow messages written in
    languages you select.
  languages: "Languages"
