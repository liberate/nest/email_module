class CreateMailboxes < ActiveRecord::Migration[7.1]
  def change
    create_table "aliases", if_not_exists: true, charset: "utf8mb4", collation: "utf8mb4_general_ci" do |t|
      t.integer  "mailbox_id"
      t.string   "source"
      t.string   "dest"
      t.boolean  "is_active",       default: true
      t.datetime "date"
      t.string   "token",           limit: 16
      t.boolean  "is_foreign",      default: false
      t.index ["mailbox_id"], name: "aliases_mailbox_id_index"
      t.index ["source"], name: "aliases_source_index"
    end

    create_table "domains", if_not_exists: true, charset: "utf8mb4", collation: "utf8mb4_general_ci" do |t|
      t.datetime "created_at"
      t.datetime "updated_at"
      t.string   "domain"
      t.boolean  "is_public",                    default: false
      t.integer  "max_users"
      t.boolean  "is_active",                    default: false
      t.integer  "default_quota"
      t.integer  "storage_policy",   limit: 1,   default: 2
      t.integer  "mfa_policy",       limit: 1,   default: 0
      t.boolean  "allow_forward",                default: true
      t.boolean  "track_changes",                default: false
      t.boolean  "may_users_invite",             default: true
      t.index ["domain"], name: "index_domains_on_domain", unique: true
    end

    create_table "expressions", if_not_exists: true, charset: "utf8mb4", collation: "utf8mb4_general_ci" do |t|
      t.integer "filter_id",      limit: 4,                        null: false
      t.string  "field_name",     limit: 20,  default: "^Subject", null: false
      t.string  "operator",       limit: 20,  default: "contains", null: false
      t.string  "expr_value",     limit: 100, default: "",         null: false
      t.boolean "case_sensitive",             default: false
      t.integer "position",       limit: 4
    end

    create_table "filters", if_not_exists: true, charset: "utf8mb4", collation: "utf8mb4_general_ci" do |t|
      t.string  "name"
      t.string  "destination_folder"
      t.integer "mailbox_id"
      t.integer "position"
      t.string  "boolean_operator", limit: 5, default: "anyof"
    end

    create_table "mail_prefs", if_not_exists: true, charset: "utf8mb4", collation: "utf8mb4_general_ci" do |t|
      t.string  "preference"
      t.text    "value"
      t.integer "mailbox_id"
      t.string  "domain"
      t.index ["domain"], name: "index_mail_prefs_on_domain"
      t.index ["mailbox_id"], name: "index_mail_prefs_on_mailbox_id"
    end

    create_table "mailboxes", if_not_exists: true, charset: "utf8mb4", collation: "utf8mb4_general_ci" do |t|
      t.integer  "user_id"
      t.integer  "server_id"
      t.integer  "primary_server_id"
      t.integer  "secondary_server_id"
      t.string   "username"
      t.string   "password"
      t.string   "domain"
      t.string   "address"
      t.string   "forward"
      t.integer  "quota"
      t.string   "maildir"
      t.datetime "last_login"
      t.boolean  "keep_backups"
      t.string   "storage_host"
      t.string   "storage_ip"
      t.boolean  "is_active",           default: true
      t.index ["address"], name: "mailboxes_address_index"
      t.index ["server_id"], name: "mailboxes_server_id_index"
      t.index ["user_id"], name: "mailboxes_user_id_index"
      t.index ["username"], name: "username_index"
    end

    create_table "ownerships", if_not_exists: true, charset: "utf8mb4", collation: "utf8mb4_general_ci" do |t|
      t.datetime "created_at"
      t.datetime "updated_at"
      t.integer  "created_by_id"
      t.integer  "domain_id"
      t.integer  "admin_id"
      t.index ["domain_id", "admin_id"], name: "index_ownerships_on_domain_id_and_admin_id", unique: true
    end

    create_table "quota", id: false, if_not_exists: true, charset: "utf8mb4", collation: "utf8mb4_general_ci" do |t|
      t.string  "username", null: false
      t.integer "bytes",    default: 0, null: false
      t.integer "messages", default: 0, null: false
    end

    create_table "servers", if_not_exists: true, charset: "utf8mb4", collation: "utf8mb4_general_ci" do |t|
      t.string  "hostname"
      t.string  "ip"
      t.string  "type"
      t.string  "status"
      t.integer "capacity"
      t.string  "ssh_key"
      t.string  "public_hostname"
      t.string  "tor_hidden_service"
      t.integer "domain_id"
      t.index ["status"], name: "servers_status_index"
      t.index ["type"], name: "servers_type_index"
    end

    create_table "storage_keys", if_not_exists: true, charset: "ascii" do |t|
      t.integer "enabled",                   limit: 1,     default: 1
      t.text    "public_key",                limit: 65535
      t.integer "pwhash_opslimit",           limit: 4
      t.integer "pwhash_memlimit",           limit: 4
      t.string  "pwhash_salt",               limit: 255
      t.string  "sk_nonce",                  limit: 255
      t.string  "recovery_nonce",            limit: 255
      t.text    "locked_secretbox",          limit: 65535
      t.text    "locked_recovery_secretbox", limit: 65535
      t.integer "user_id",                   limit: 4
      t.integer "pwhash_algo",               limit: 4,     default: 0
      t.index ["user_id"], name: "index_storage_keys_on_user_id"
    end

  end
end