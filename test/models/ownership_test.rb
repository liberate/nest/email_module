require File.expand_path("../../test_helper.rb", __FILE__)

class OwnershipTest < ActiveSupport::TestCase

  def test_ownership_association
    assert users(:alice_foo).domains.include?(domains(:foo))
    assert domains(:foo).admins.include?(users(:alice_foo))
  end

  def test_domain_admins
    foo = domains(:foo)
    assert users(:alice_foo).admin_of?(foo)
    refute users(:bob_foo).admin_of?(foo)
  end

  def test_is_superadmin?
    Conf.tmp(:admins, ['red@example.org']) do
      assert users(:red).is_superadmin?
      assert users(:alice_foo).is_admin?
      refute users(:alice_foo).is_superadmin?
    end
  end

  def test_superadmins_always_admin
    Conf.tmp(:admins, ['red@example.org']) do
      Domain.all.each do |domain|
        assert users(:red).admin_of? domain
      end
    end
  end

end
