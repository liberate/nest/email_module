require File.expand_path("../../test_helper.rb", __FILE__)

class AliasTest < ActiveSupport::TestCase

  def test_create
    mb = create_mailbox 'kangaroo'
    mbalias = mb.aliases.create!(source: 'joey@'+Conf.domain, dest: mb.dest)
    assert mbalias.is_active?
    assert mbalias.is_confirmed?
    assert !mbalias.is_foreign?
    mbalias = mb.aliases.create!(source: 'joey@example.com', dest: mb.dest)
    assert !mbalias.is_active?
    assert !mbalias.is_confirmed?
    assert mbalias.is_foreign?
  end

  def test_create_invalid
    mb = create_mailbox 'koala'
    mbalias = mb.aliases.create(source: 'bear', dest: mb.dest)
    assert !mbalias.valid?
    mbalias = mb.aliases.create(source: 'bear--x@'+Conf.domain, dest: mb.dest)
    assert !mbalias.valid?
    mbalias = mb.aliases.create(source: 'bear!@'+Conf.domain, dest: mb.dest)
    assert !mbalias.valid?
    mbalias = mb.aliases.create(source: 'red@'+Conf.domain, dest: mb.dest)
    assert !mbalias.valid?
  end

  def test_create_monopoly
    Conf.tmp(:forbidden_alias_domains, ['gmail.com']) do
      mb = create_mailbox 'marmot'
      mbalias = mb.aliases.create(source: 'fuzz_bucket@gmail.com', dest: mb.dest)
      assert !mbalias.valid?
      assert_equal "The email provider gmail.com requires that you use their system for all aliases",
        mbalias.errors[:base].first
    end
  end

  def test_create_forbidden
    AuthApp.forbidden_usernames = ['root']
    mb = create_mailbox 'wallaby'
    mbalias = mb.aliases.create(source: 'root@'+Conf.domain, dest: mb.dest)
    assert !mbalias.valid?
    assert_equal "Alias root@example.org is already taken", mbalias.errors[:base].first, 'must honor forbidden usernames'
  end

  def test_create_limit
    mb = create_mailbox 'dingo'
    Conf.tmp(:max_aliases, 1) do
      bad = mb.aliases.create(source: 'pups@' + Conf.domain, dest: mb.dest)
      assert !bad.valid?
      assert_equal "Too many aliases", bad.errors[:base].first
    end
  end

  def test_create_aliases_on_native_domains
    mb = create_mailbox 'black-crested-cockatoo'

    # public domain
    public_domain = Domain.create domain: 'downunder.country', is_public: true
    good = mb.aliases.create(source: 'wing@downunder.country', dest: mb.dest)
    assert good.valid?

    # private domain
    private_domain = Domain.create domain: 'oz.country', is_public: false
    bad = mb.aliases.create(source: 'wing@oz.country', dest: mb.dest)
    assert !bad.valid?
    assert_equal "You do not have permission to create aliases on domain oz.country", bad.errors[:base].first

    # private with same mailbox domain
    mb2 = create_mailbox 'black-crested-cockatoo', "oz.country"
    good = mb2.aliases.create(source: 'wing@oz.country', dest: mb2.dest)
    assert good.valid?

    # private domain with admin
    user = User.create! username: 'tawny-frogmouth', domain: Conf.domain, password: '621783901283'
    user.ownerships.create!(domain: private_domain)
    assert user.admin_of?(private_domain)
    mb3 = user.make_mailbox
    mb3.save!
    good = mb3.aliases.create(source: 'marbled-frogmouth@oz.country', dest: mb.dest)
    assert good.valid?, "domain admins can add aliases for themself"
  end

end
