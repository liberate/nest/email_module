require File.expand_path("../../test_helper.rb", __FILE__)

class MailboxTest < ActiveSupport::TestCase

  def setup
  end

  def test_mailbox_creation
    create_mailbox('kangaroo')
    assert_equal 'kangaroo@'+Conf.domain, @mailbox.address, 'address must be set'
    assert @mailbox.aliases.first, 'there should be aliases'
    assert_equal @mailbox.address, @mailbox.aliases.first.source, 'alias source should match address'
    assert_equal @mailbox.address, @mailbox.aliases.first.dest, 'alias dest should match address (%s)' % @mailbox.aliases.first.dest
  end

  def test_mailbox_server
    server = Mailbox::Server.next_available service: 'imap', domain: Conf.domain
    assert server, 'there should be an available server'
    oldsize = server.mailboxes.size
    create_mailbox
    assert_equal server, @mailbox.server, 'mailbox server should be set'
    assert_equal server.hostname, @mailbox.storage_host, 'mailbox storage host should be set'
    assert_equal oldsize+1, server.mailboxes.reload.size, 'server mailboxes size should increment'
  end

  def test_mailbox_server_with_domain
    server = Mailbox::Server.order("capacity ASC").limit(1).take

    # normally, server is not picked
    server_picked = Mailbox::Server.next_available(service: 'imap')
    assert_not_equal server.id, server_picked.id

    server.domain_id = domains(:foo).id
    server.save

    # when limited by domain, server is picked
    server_picked = Mailbox::Server.next_available service: 'imap', domain: domains(:foo).domain
    assert_equal server.id, server_picked.id
  end

  def test_assign_aliases
    create_mailbox('emma')
    @mailbox.aliases.create_many ["goldman@#{Conf.domain}", "redemma@#{Conf.domain}"]
    assert_equal 0, @mailbox.errors.size, @mailbox.errors.full_messages.join(', ')
    assert_equal 3, @mailbox.aliases.size
    assert_equal 'emma@' + Conf.domain, @mailbox.aliases[0].source
    assert_equal 'goldman@' + Conf.domain, @mailbox.aliases[1].source
    assert_equal 'redemma@' + Conf.domain, @mailbox.aliases[2].source

    @mailbox.aliases.create_many
    assert_equal 1, @mailbox.aliases.size
  end

  def test_alias_errors
    create_mailbox('badbluejay')
    Conf.tmp(:forbidden_alias_domains, ['bad.com']) do
      assert_raises ActiveRecord::RecordInvalid do
        @mailbox.aliases.create_many ["x@#{Conf.domain}", "xxx@bad.com", "xxx@#{Conf.domain}", "ⓐⓐⓐ@#{Conf.domain}"]
      end
    end
    assert_equal 3, @mailbox.errors[:aliases].size
    assert_equal "Alias x@example.org is too short", @mailbox.errors[:aliases][0]
    assert_equal "The email provider bad.com requires that you use their system for all aliases", @mailbox.errors[:aliases][1]
    assert_equal "Alias ⓐⓐⓐ@#{Conf.domain} is invalid", @mailbox.errors[:aliases][2]

    assert_raises ActiveRecord::RecordInvalid, 'do not allow duplicate aliases' do
      @mailbox.aliases.create! source: "badbluejay@#{Conf.domain}", dest: @mailbox.dest
    end
  end

  def test_mailbox_forwarding
    create_mailbox('balladier')
    @mailbox.aliases.create_many ['philochs@example.org']
    @mailbox.update_forward! 'joehill@example.com'
    assert_equal 2, @mailbox.aliases.size, 'there are the wrong number of aliases'
    @mailbox.aliases.each do |a|
      assert_equal 'joehill@example.com', a.dest, 'aliases should be updated when forward set'
      assert a.source == 'philochs@example.org' || a.source == 'balladier@example.org', 'alias source is wrong'
    end
    assert_raises ActiveRecord::RecordInvalid, 'cannot set a forward the same as an alias' do
      @mailbox.update_forward! 'philochs@example.org'
    end
    assert_raises ActiveRecord::RecordInvalid, 'cannot set forward to address that does not exist' do
      @mailbox.update_forward! "noaddress@#{Conf.domain}"
    end

    @mailbox.update_forward! 'billybragg@example.net'
    @mailbox.aliases.reload
    @mailbox.aliases.each do |a|
      assert_equal 'billybragg@example.net', a.dest, 'aliases should be updated when forward set'
    end

    @mailbox.update_forward! ''
    @mailbox.aliases.reload
    @mailbox.aliases.each do |a|
      assert_equal 'balladier@' + Conf.domain, a.dest, 'aliases should be updated when forward is removed'
    end
  end

  def test_update
    create_mailbox('yes_bird')
    @mailbox.update_username!('no_bird')
    assert @mailbox.valid?, 'should be valid'
    assert_equal ['no_bird',Conf.domain].join('@'), @mailbox.address, 'address should be updated'
    aliases = @mailbox.aliases.collect{|a| a.source}
    assert aliases.include? "yes_bird@#{Conf.domain}"
    assert aliases.include? "no_bird@#{Conf.domain}"
  end

  def test_destroy
    create_mailbox
    id = @mailbox.id
    @mailbox.destroy
    assert_nil Mailbox.find_by(id: id)
  end

  def test_user_creation_of_mailbox
    u = User.new username: 'joehill', domain: 'example.org', password: 'workers of the world unite'
    u.make_mailbox
    u.save!
    assert u.persisted?, u.errors.full_messages.join(', ')
    assert u.mailbox.persisted?, u.mailbox.errors.full_messages.join(', ')
    assert_equal "joehill@example.org", u.mailbox.address
    assert_match /^\{ARGON2I\}/, u.mailbox.password, "mailbox password digests must start with {ARGON2}"

    # now, lets make the validation to fail.
    assert_raises ActiveRecord::RecordInvalid do
      u.mailbox.update_username!(nil)
    end
  end

  def test_rename
    mbalias = "purple_lizard@#{Conf.domain}"
    newname = "renamed@#{Conf.domain}"
    oldname = "green_lizard@#{Conf.domain}"

    create_mailbox('green_lizard')
    @mailbox.aliases.create_many [mbalias]
    @mailbox.update_username!('renamed')
    assert_equal 3, @mailbox.aliases.size, 'there should be three aliases'
    aliases = @mailbox.aliases.collect{|a| a.source}
    assert aliases.include?(newname), aliases.inspect
    assert aliases.include?(mbalias), aliases.inspect
    assert aliases.include?(oldname), aliases.inspect
    @mailbox.aliases.each do |a|
      assert_equal newname, a.dest, 'alias dest should be updated when mailbox is renamed'
    end
  end

  def test_aliases_atomic_change
    create_mailbox
    @mailbox.aliases.create_many ["good-address1@#{Conf.domain}"]
    original_sources = @mailbox.aliases.sources
    assert_raises ActiveRecord::RecordInvalid do
      @mailbox.aliases.create_many ["bad-address good-address2@#{Conf.domain}"]
    end
    assert_equal original_sources, @mailbox.reload.aliases.sources
  end

  def test_add_taken_aliases
    # some users don't have mailboxes. This is OK. you should still not be
    # able to create aliases with their username.
    create_mailbox
    user = User.create! username: 'wallaby', domain: Conf.domain
    assert_raises ActiveRecord::RecordInvalid do
      @mailbox.aliases.create_many ["wallaby@" + Conf.domain]
    end
  end

end
