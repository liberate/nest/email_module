# test using the environment of the parent app
require_relative '../../../test/test_helper'

module ActiveSupport
  class TestCase
    def create_mailbox(username='kangaroo', domain=Conf.domain)
      @user    = User.create! username: username, domain: domain
      @mailbox = Mailbox.create! user: @user
    end
  end
end

# class ActionController::TestCase
#   def act_as(user)
#     session[:user_id] = user ? user.id : nil
#   end

#   def assert_text(text)
#     assert_select "body", /#{text}/
#   end

#   def refute_text(text)
#     assert_select "body" do |body|
#       assert_no_match text, body.to_s
#     end
#   end
# end

# class ActionDispatch::IntegrationTest
#   include Capybara::DSL
#   include Capybara::Assertions

#   def login_with(login, password: 'password')
#     reset_session!
#     visit root_path
#     fill_in 'login', with: login
#     fill_in 'password', with: password
#     click_button "Log In"
#   end

#   def logout
#     click_button "Log Out" if find_all('.logout').present?
#   end

#   def refresh_page
#     visit [current_path, page.driver.request.env['QUERY_STRING']].reject(&:blank?).join('?')
#   end

#   def search_for(term)
#     fill_in 'search', with: term
#     click_button "Search"
#   end
# end
