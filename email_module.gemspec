Gem::Specification.new do |spec|
  spec.name        = "email_module"
  spec.version     = "0.1.0"
  spec.authors     = ["Nest Developers"]
  spec.summary     = "Manage email accounts in Nest"
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    Dir["{app,config,db,lib}/**/*"]
  end

  spec.add_dependency "paper_trail"
  spec.add_dependency "acts_as_list"
end
