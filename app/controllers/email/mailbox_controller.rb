class MailboxController < ApplicationController
  before_filter :fetch_mailbox

  def show
    if @mailbox.nil?
      redirect_to new_mailbox_url
    end
  end

  def new
    @mailbox = Mailbox.new
  end

  def create
    current_user.make_mailbox
    current_user.save!
    redirect_to mailbox_url
  end

  def update
    if !params[:mailbox][:forward].nil?
      @mailbox.update_forward! params[:mailbox][:forward]
    end
    @mailbox.save!
  rescue ActiveRecord::RecordInvalid
  ensure
    if @mailbox.errors.empty?
      flash[:success] = t(:changes_saved)
      redirect_to mailbox_url
    else
      flash.now[:danger] = @mailbox.errors
      render action: 'show'
    end
  end

  protected

  def fetch_mailbox
    @mailbox = current_user.mailbox
  end

end
