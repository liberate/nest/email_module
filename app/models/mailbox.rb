# == Schema Information
#
# Table name: mailboxes
#
#  id                  :integer          not null, primary key
#  user_id             :integer
#  server_id           :integer
#  primary_server_id   :integer
#  secondary_server_id :integer
#  username            :string
#  password            :string
#  domain              :string
#  address             :string
#  forward             :string
#  quota               :integer
#  maildir             :string
#  last_login          :datetime
#  keep_backups        :boolean
#  storage_host        :string
#  storage_ip          :string
#  is_active           :boolean          default(TRUE)
#

class Mailbox < ActiveRecord::Base

  has_paper_trail(
    if: proc {|mb| mb.domain_object&.track_changes? },
    meta: {
      user_id: :user_id,
      domain_id: :domain_id
    },
    only: [
      :forward, :quota, :is_active, :storage_host, :username, :domain
    ],
  )

  ##
  ## ASSOCIATIONS
  ##

  belongs_to :user
  belongs_to :server
  has_many :aliases, -> { extending Mailbox::Alias::Extension }, dependent: :delete_all
  has_many :mail_prefs, -> { extending Mailbox::MailPref::Extension }, dependent: :delete_all
  has_many :filters, -> { order("position") }, dependent: :delete_all

  ##
  ## VALIDATIONS
  ##

  validates_presence_of :username, :domain
  validates_uniqueness_of :address
  validates_email_format_of :forward, allow_blank: true, allow_nil: true
  validate :validate_forward

  ##
  ## EVENT HOOKS
  ##

  before_validation :update_address
  before_validation :seed_initial_values
  after_create      :send_welcome_email_task
  after_create      :set_maildir
  after_destroy     :destroy_mailbox_task

  ##
  ## ACCESSORS
  ##

  def dest
    return forward if forward.present?
    return address
  end

  #
  # Dovecot wants quota to be represented in bytes,
  # but for display and configuration we use megabytes.
  #
  def quota
    read_attribute('quota').to_i / 1.megabyte
  end
  def quota_bytes
    read_attribute('quota')
  end
  def quota=(val)
    write_attribute('quota', [val.to_i * 1.megabyte, 2**63].min )
  end
  def quota_bytes=(val)
    write_attribute('quota', [val.to_i, 2**63].min )
  end

  def password=(new_password)
    if new_record?
      write_attribute(:password, add_digest_algorithm(new_password))
    else
      raise ArgumentError, 'attempt to change password: use update_password_digest!()'
    end
  end

  # return true if the maildir path is numeric (new style) or based on domain/username (old style)
  def numeric_path?(dir=self.maildir)
    dir&.count('/') == 1
  end

  #
  # Immediately saves a new password digest for this mailbox.
  #
  # `new_digest` is the hashed value, NOT the cleartext password.
  #
  def update_password_digest!(new_digest)
    self.update_column(:password, add_digest_algorithm(new_digest))
  end

  #
  # we copy data from server to mailbox so that we don't have to do a join
  # with the server table for each message delivered.
  #
  def server=(newserver)
    if newserver
      self.storage_host = newserver.hostname
      self.storage_ip = newserver.ip
      self.server_id = newserver.id
    else
      self.storage_ip = nil
      self.server_id = nil
    end
  end

  def domain_id
    domain_object&.id
  end

  def domain_object
    @domain_object ||= Domain.find_by(domain: domain)
  end

  ##
  ## TASKS
  ##

  def restore_backups_task
    RestoreMailboxTask.create :maildir => maildir, :hostname => storage_host, :username => username, :snapshot => 'daily'
    RestoreMailboxTask.create :maildir => maildir, :hostname => storage_host, :username => username, :snapshot => 'weekly.1'
  end

  #
  # if there is a pending destroy task, ensure that the create is run afterwards
  #
  def send_welcome_email_task
    SendWelcomeEmailTask.create(hostname: storage_host, username: username, domain: domain) if domain == Conf.domain
  end

  def destroy_mailbox_task(recreate: false)
    destroy_task = DestroyMailboxTask.create :maildir => maildir, :hostname => storage_host, :username => username, :domain => domain

    if recreate
      EnableUserTask.create :hostname => storage_host, :user_id => self.user_id
    end

    DestroyDovecotDataTask.create :username => username, :hostname => storage_host, :domain => domain
    DestroyRoundCubeDataTask.create :username => username, :hostname => storage_host
    DestroyBackupDataTask.create :maildir => maildir, :domain => domain, :username => username, :hostname => Conf.backup_host
  end

  #
  # The mailbox and user are disabled while a rename is in progress.
  # The EnableUserTask is run last, to re-enable everything.
  #
  def rename_mailbox_task(old_username: nil, old_maildir: nil)
    parent = nil
    if !numeric_path?(old_maildir)
      parent = DestroyDovecotDataTask.create(
        :hostname => storage_host, :username => old_username,
        :domain => domain
      )
      parent = RenameMailboxTask.create(
        :parent => parent,
        :hostname => storage_host, :username => username,
        :old_maildir => old_maildir, :new_maildir => maildir
      )
    end
    parent = RenameUserTask.create(
      :parent => parent,
      :hostname => storage_host,
      :old_username => old_username, :new_username => username
    )
    EnableUserTask.create(
      :parent => parent, :hostname => storage_host,
      :user_id => self.user_id
    )
  end

  def migrate_task(new_host)
    pause
    t1 = PushMailboxTask.create(hostname: storage_host, dest_hostname: new_host,
      path: self.maildir, username: self.username)
    t2 = PickupMailboxtask.create(parent: t1, hostname: new_host, path: self.maildir)
    EnableUserTask.create(parent: t2, hostname: new_host, user_id: self.user_id) # also enables mailbox
  end

  ##
  ## ACTIONS
  ##

  def close
    self.aliases.close_all
    self.update_attribute('is_active', false)
  end

  def restore
    self.aliases.restore_all
    self.update_attribute('is_active', true)
  end

  def pause
    self.update_attribute('is_active', false)
  end

  def purge
    self.destroy_mailbox_task(recreate: true)
    self.mail_prefs.destroy_all
    self.filters.destroy_all # important that filter destroy() is called
    Mailbox::Alias.transaction do
      self.aliases.destroy_all
      self.aliases.create_default
    end
  end

  # before_validation
  def update_address
    if username && (username_changed? || domain_changed? ||
                    self.address.blank? || self.maildir.blank?)
      self.address = [username, domain].join('@')
      if maildir.present? && !numeric_path?
        self.maildir = [domain, username.first, username].join('/')
      end
    end
  end

  def username=(username)
    if new_record?
      write_attribute(:username, username)
    else
      raise ArgumentError, 'attempt to change username: use update_username!()'
    end
  end

  def update_username!(new_username)
    Mailbox.transaction do
      old_address  = self.address
      old_username = self.username
      old_maildir  = self.maildir
      write_attribute(:is_active, false)
      write_attribute(:username, new_username)
      save!
      aliases.create_default
      aliases.update_forward!
      rename_mailbox_task(old_username: old_username, old_maildir: old_maildir)
    end
  end

  def forward=(address)
    raise ArgumentError, 'attempt to change forward: use update_forward!()'
  end

  def update_forward!(new_address)
    if "" == new_address
      new_address = nil
    end
    if read_attribute('forward') == new_address
      return
    end
    Mailbox.transaction do
      write_attribute('forward', new_address)
      save!
      aliases.update_forward!
    end
  end

  def self.update_default_quota(domain: nil, quota: nil)
    quota_bytes = [quota.to_i * 1.megabyte, 2**63].min
    safe_execute(
      "UPDATE mailboxes SET quota = ? WHERE quota < ? AND domain = ?",
      quota_bytes, quota_bytes, domain.domain
    )
  end

  protected

  #
  # Some rules:
  #
  # * You cannot set a forward to your own NATIVE alias
  # * You can set a forward for any FOREIGN alias, unless forbidden by domain policy.
  # * You can set the forward to another user's NATIVE alias, iff that alias exists.
  #
  def validate_forward
    myalias = self.aliases.find_by(source: self.forward)
    if myalias && !myalias.is_foreign?
      self.errors.add(:forward, I18n.t('errors.messages.invalid'))
      self.errors.add(:forward, I18n.t(:forward_error_alias))
    end
    if self.forward =~ /@#{Regexp.escape(Conf.domain)}\z/
      unless Mailbox::Alias.find_by(source: self.forward)
        self.errors.add(:forward, I18n.t('errors.messages.invalid'))
        self.errors.add(:forward, I18n.t(:forward_error_exist))
      end
    end
    if self.forward&.present? && !domain_object.allow_forward?
      # note: exists?() parameters are not sanitized unless strings
      unless Domain.exists?(domain: self.forward.split('@')&.last)
        self.errors.add(:forward, I18n.t('errors.messages.invalid'))
        self.errors.add(:forward, I18n.t(:forbidden_by_policy))
      end
    end
  end

  private

  #
  # dovecot requires that we specify the digest algo by prefixing "{ALGO}" to
  # the digest, if the digest algorithm is not the default one.
  #
  def add_digest_algorithm(digest)
    if digest =~ /\A\$argon2id?\$/
      "{ARGON2I}" + digest
    else
      digest
    end
  end

  def default_mail_prefs
    lang_spam_pref    = MailPref.new(:preference => 'ok_languages', :value => 'all')
    locales_spam_pref = MailPref.new(:preference => 'ok_locales', :value => 'all')
    bayes_spam_pref   = MailPref.new(:preference => 'use_bayes', :value => '1')
    return [lang_spam_pref, locales_spam_pref, bayes_spam_pref]
  end

  ##
  ## CALLBACKS
  ##

  # before_validation
  def seed_initial_values
    if new_record?
      if user
        self.username = user.username
        self.domain   = user.domain
        update_address
      end
      self.aliases.build :source => address, :dest => address
      self.server ||= Server.next_available service: 'imap', domain: self.domain
      self.keep_backups = true
      self.mail_prefs << default_mail_prefs
      domain_record = Domain.find_by(domain: self.domain)
      if domain_record && !domain_record.is_public?
        self.quota = domain_record.default_quota
      else
        self.quota = Conf.default_quota
      end
    end
  end

  # after_create
  def set_maildir
    update_attribute(:maildir, [sprintf("%02d", self.id)[-2..-1], self.id].join('/'))
  end

end
