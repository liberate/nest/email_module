# == Schema Information
#
# Table name: filters
#
#  id                 :integer          not null, primary key
#  name               :string
#  destination_folder :string
#  mailbox_id         :integer
#  position           :integer
#  boolean_operator   :string(5)        default("anyof")
#

#
# Fields:
#
# name
# destination_folder
# mailbox_id
# position
# boolean_operator
#

class Mailbox::Filter < ActiveRecord::Base
  self.table_name = 'filters'

  acts_as_list :scope => :mailbox
  has_many :expressions, -> { order("position") }, dependent: :delete_all
  belongs_to :mailbox

  validates :name, presence: true
  validates :destination_folder, presence: true
  validates :boolean_operator, inclusion: { in: ['allof', 'anyof'] }

  before_save :clean_folder
  def clean_folder
    self.destination_folder = self.destination_folder.gsub(/\.{2,}/, '').gsub('/','').gsub('"','').gsub("'",'')
  end

  def update_expressions!(exprs)
    self.class.transaction do
      self.expressions.clear
      exprs.each do |index, expr|
        next if expr[:expr_value].empty?
        expr[:position] = index
        self.expressions.build(
          expr.permit(:field_name, :operator, :expr_value)
        )
      end
      self.save!
    end
  end

end
