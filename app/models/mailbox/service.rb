# == Schema Information
#
# Table name: services
#
#  id          :integer          not null, primary key
#  name        :string
#  description :string
#

#
# DEPRECATED
#

class Mailbox::Service < ActiveRecord::Base
  self.table_name = 'services'

  has_and_belongs_to_many :servers

  def self.servers_with(service_name)
    service = Service.find_by_name(service_name)
    return nil unless service
    return service.servers
  end
end
