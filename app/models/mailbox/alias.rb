# == Schema Information
#
# Table name: aliases
#
#  id         :integer          not null, primary key
#  mailbox_id :integer
#  source     :string
#  dest       :string
#  is_active  :boolean          default(TRUE)
#  date       :datetime
#  token      :string(16)
#  is_foreign :boolean          default(FALSE)
#

#
# A mail alias defines a mapping from source email address to destination
# email address.
#


class Mailbox::Alias < ActiveRecord::Base
  self.table_name = 'aliases'

  # Require number or letter as first digit, don't allow two symbols in a row.
  # There are many valid characters for email addresses. However, allowing
  # weird characters creates a potential security risk, because it allows
  # someone to register an alias that appears similar to a different user.
  USERPART_RE = /\A[a-z0-9]+([-_\.]?[a-z0-9])+\z/

  # these values are based on the schema:
  MAX_ALIAS_LENGTH = 255
  MIN_ALIAS_LENGTH = 2
  CODE_LENGTH = 16

  belongs_to :mailbox

  validates_email_format_of :dest

  validates :dest,
    presence: true,
    length: { maximum: MAX_ALIAS_LENGTH }

  validate :validate_source

  attr_accessor :add_errors_to

  before_create :initialize_active_state

  has_paper_trail(
    if: proc {|a| a.mailbox.domain_object&.track_changes? },
    meta: {
      user_id: proc {|a| a.mailbox.user_id},
      domain_id: proc {|a| a.mailbox.domain_id}
    },
    only: [:source, :dest, :is_active]
  )

  ##
  ## ATTRIBUTES
  ##

  def is_confirmed?
    self.token.nil?
  end

  def source_userpart
    (self.source || "").split('@').first
  end

  def source=(value)
    write_attribute(:source, value)
    write_attribute(:is_foreign , !is_native?)
  end

  def display_name
    self.source
  end

  ##
  ## ACTIONS
  ##

  def close
    update_attribute('is_active', false)
  end

  def restore
    update_attribute('is_active', true)
  end

  #
  # confirm that the user really does own the email
  # address in self.source
  #
  def confirm
    if self.mailbox.is_active?
      self.is_active = true
    end
    self.token = nil
    self.save
  end

  def is_confirmed?
    self.token.nil?
  end

  def source_userpart
    (self.source || "").split('@').first
  end

  protected

  #
  # "Native" aliases are aliases on domains that this provider hosts.
  # This is protected. Most code should instead access the public
  # Alias#is_foreign? attribute.
  #
  def is_native?
    domain_object.present?
  end

  def domain_object
    @domain ||= Domain.find_by_domain(self.source&.split('@')&.last)
  end

  def initialize_active_state
    if self.is_foreign
      self.is_active = false
      self.token = RandomCode.create(CODE_LENGTH)
    else
      self.is_active = true
      self.token = nil
    end
    true
  end

  #
  # Ensure that source is good.
  #
  def validate_source
    if self.mailbox && self.mailbox.aliases.count >= Conf.max_aliases
      # The UI should never allow this to be reached, so no translation needed
      add_error("Too many aliases")
      return
    end

    if self.source.nil? || self.source.empty?
      add_error(I18n.t(:alias_error_missing))
      return
    end

    if !self.new_record? && !self.source_changed?
      # allow older aliases that may not conform to the new rules
      return
    end

    self.source = self.source.downcase
    userpart, domainpart = self.source.split('@')
    userpart ||= ''
    domainpart ||= ''
    if self.is_native?
      if !domain_object.is_public? && mailbox.domain != domainpart && !mailbox&.user&.admin_of?(domain_object)
        add_error(I18n.t(:alias_error_forbidden, domain: domainpart))
      elsif userpart.length < MIN_ALIAS_LENGTH
        add_error(I18n.t(:alias_error_length_short, alias: self.source))
      elsif self.source =~ /[\+\|]/
        add_error(I18n.t(:alias_error, alias: self.source))
      elsif userpart !~ USERPART_RE
        add_error(I18n.t(:alias_error, alias: self.source))
      elsif !User.username_allowed?(userpart)
        add_error(I18n.t(:alias_error_taken, alias: self.source))
      elsif User.where("username = ? AND domain = ? AND id != ?", userpart, domainpart, self.mailbox.user_id).take
        add_error(I18n.t(:alias_error_taken, alias: self.source))
      elsif ReservedUsername.where(username: userpart, domain: domainpart).take
        add_error(I18n.t(:alias_error_taken, alias: self.source))
      elsif Mailbox::Alias.where(source: self.source).take
        add_error(I18n.t(:alias_error_taken, alias: self.source))
      end
    else
      if Conf.forbidden_alias_domains&.include?(domainpart)
        add_error(I18n.t(:alias_error_domain, domain: domainpart))
      end
    end
    if !has_errors?
      if self.source.length > MAX_ALIAS_LENGTH
        add_error(I18n.t(:alias_error_length_long, alias: self.source))
      end
      if ValidatesEmailFormatOf::validate_email_format(self.source) != nil
        add_error(I18n.t(:alias_error_email, alias: self.source))
      end
    end
  end

  def has_errors?
    if self.add_errors_to == :mailbox
      self.mailbox.errors.any?
    else
      self.errors.any?
    end
  end

  def add_error(msg)
    if self.add_errors_to == :mailbox
      self.mailbox.errors.add(:aliases, msg)
    else
      self.errors.add(:base, msg)
    end
  end

  #
  # This module is loaded in the association mailbox.aliases
  # e.g. mailbox.aliases.update_sources! 'blah@example.org'
  #
  module Extension

    def mailbox
      self.proxy_association.owner
    end

    #
    # creates the default required alias for a mailbox.
    #
    def create_default
      mailbox.update_address
      if !Mailbox::Alias.where(source: mailbox.address).take
        self.create(
          mailbox: mailbox,
          source: mailbox.address,
          dest: mailbox.dest,
          add_errors_to: :mailbox
        )
      end
    end

    def update_forward!
      Mailbox::Alias.transaction do
        self.each do |a|
          a.dest = mailbox.dest
          a.save!
        end
      end
    end

    #
    # lets you assign aliases as a whitespace or comma separated list of emails.
    # changes are saved immediately.
    #
    # WARNING: this will destroy all the current aliases. This should not
    # be only used when initially seeding the list of aliases.
    #
    def create_many(new_addresses=[])
      raise ArgumentError unless new_addresses.is_a?(Array)
      Mailbox::Alias.transaction do
        mailbox.update_address
        self.clear
        new_alias = self.create(
          mailbox: mailbox,
          source: mailbox.address,
          dest: mailbox.dest,
          add_errors_to: :mailbox
        )
        new_addresses.each do |source_addr|
          source_addr = source_addr.strip
          if !source_addr.empty? && source_addr != mailbox.address
            new_alias = self.create(
              mailbox: mailbox,
              source: source_addr,
              dest: mailbox.dest,
              add_errors_to: :mailbox
            )
          end
        end
        if mailbox.errors[:aliases].any?
          raise ActiveRecord::RecordInvalid, mailbox
        end
      end
    end

    #
    # a string of all the aliases, one per line
    #
    def sources
      self.collect {|a| a.source}.join("\n")
    end

    #
    # the aliases, excluding the address specified
    #
    def sources_without(address)
      self.collect {|a|
        a.source == address ? nil : a.source
      }.compact.join("\n")
    end

    def close_all
      self.each {|a| a.close}
    end

    def restore_all
      self.each {|a| a.restore}
    end
  end

end

