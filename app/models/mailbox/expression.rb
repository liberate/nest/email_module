# == Schema Information
#
# Table name: expressions
#
#  id             :integer          not null, primary key
#  filter_id      :integer          not null
#  field_name     :string(20)       default("^Subject"), not null
#  operator       :string(20)       default("contains"), not null
#  expr_value     :string(100)      default(""), not null
#  case_sensitive :boolean          default(FALSE)
#  position       :integer
#

#
# A mail filter expression
#
#
# integer  filter_id
# string   field_name
# string   operator
# string   gexpr_value
# boolean  case_sensitive
# integer  position
#

class Mailbox::Expression < ActiveRecord::Base
  self.table_name = 'expressions'

  acts_as_list :scope => :filter
  belongs_to :filter

  validates :operator, inclusion: {
    in: ['contains', 'starts', 'ends', 'exactly']
  }

  validates :field_name, inclusion: {
    in: ['From', 'To', 'CC', 'Subject', 'Body', 'List-Id']
  }
end

