# == Schema Information
#
# Table name: servers
#
#  id                 :integer          not null, primary key
#  hostname           :string
#  ip                 :string
#  type               :string
#  status             :string
#  capacity           :integer
#  ssh_key            :string
#  public_hostname    :string
#  tor_hidden_service :string
#

#
# capacity: an integer value representing the relative capacity of this
# server in relation to the other servers of the same service type.
#
# status is one of [waking, alive, dying, dead]
#
# TODO: remove type from servers table?
#

class Mailbox::Server < ActiveRecord::Base
  self.table_name = "servers"

  has_many :mailboxes
  has_many :primary_mailboxes, :foreign_key => 'primary_server_id', :class_name => 'Mailbox'
  has_many :secondary_mailboxes, :foreign_key => 'secondary_server_id', :class_name => 'Mailbox'
  has_and_belongs_to_many :services
  belongs_to :domain

  # unique IPs are currently required because of how we handle changes in IP address
  # see after_save(). but this does not need to be the case.
  validates_uniqueness_of(:ip)
  validates_format_of(:ip, :with => /\A(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\z/)
  validates_presence_of(:hostname)

  validates :capacity, numericality: { less_than: 2**31 }

  def services_display
    services.collect{|s|s.name}.join(', ')
  end

  def available; Integer(read_attribute('available')); end

  def add_service!(service)
    self.services << service unless self.services.include? service
  end

  def public_hostname
    return read_attribute(:public_hostname) if read_attribute(:public_hostname).present?
    return read_attribute(:hostname)
  end

  ##
  ## CLASS METHODS
  ##

  #
  # This only supports IMAP servers
  #
  def self.next_available(service: 'imap', domain: nil)
    domain_limit = ""
    if service == 'imap'
      if domain
        domain_id = Domain.find_by_domain(domain).id
        if exists?(domain_id: domain_id)
          domain_limit = "AND servers.domain_id = #{domain_id}"
        end
      end
      servers = Mailbox::Server.find_by_sql(%Q[
        SELECT servers.*, servers.capacity - count(mailboxes.id) AS available
        FROM servers
        LEFT OUTER JOIN mailboxes ON servers.id=mailboxes.server_id
        WHERE servers.status = 'alive'
        #{domain_limit}
        GROUP BY servers.id
        ORDER BY available DESC])
    else
      raise ArgumentError, 'only IMAP service supported'
    end
    return servers.first
  end

  private

  ##
  ## CALLBACKS
  ##

  def before_destroy
    if mailboxes.size > 0
      errors.add_to_base("Cannot delete a server with active mailboxes")
      return false
    elsif status != 'dead'
      errors.add_to_base("Cannot delete a server that is not dead")
      return false
    else
      return true
    end
  end

  # TODO: write a test for this
  def after_save
    if ip_changed?
      # we need to update the cached IP in all the mailboxes.
      # this only works if IP addresses are unique.
      old_ip = ip_was
      if old_ip.present? and self.ip.present?
        connection.execute "UPDATE mailboxes SET storage_ip = '%s' WHERE storage_ip = '%s'" % [self.ip, old_ip]
      end
    end
    if hostname_changed?
      old = hostname_was
      if old.present? and hostname.present?
        connection.execute "UPDATE mailboxes SET storage_host = '%s' WHERE storage_host = '%s'" % [self.hostname, old]
      end
    end
  end

end
