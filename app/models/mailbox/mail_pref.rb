#
# A simple key-value store for mailbox preferences
#
# Table name: mail_prefs
#
#  id         :integer          not null, primary key
#  preference :string(30)
#  value      :string(150)
#  mailbox_id :integer
#
class Mailbox::MailPref < ActiveRecord::Base
  self.table_name = "mail_prefs"

  validates :value, length: { maximum: 65535 }
  belongs_to :mailbox

  module Extension
    def get(*keys)
      where(preference: keys).map {|i| [i.preference, i.value]}.to_h.with_indifferent_access
    end
    def set(key_value_hash)
      key_value_hash.each do |key,value|
        if value && !value&.empty?
          where(preference: key).first_or_create.update_attribute(:value, value)
        else
          where(preference: key).delete_all
        end
      end
    end
  end

  extend Extension
end
