#
# User methods for dealing with mailboxes
#

module Email
  module UserMailbox
    extend ActiveSupport::Concern

    included do
      has_one :mailbox, dependent: :destroy
      has_one :storage_key, dependent: :destroy
    end

    #
    # create a mailbox record
    #
    # This should be the only place that a mailbox gets built.
    #
    # TODO: support mailbox_secret
    #
    def make_mailbox
      new_mailbox_password = primary_secret || User.new_digest(password)
      build_mailbox username: username,
                    domain:   domain,
                    password: new_mailbox_password
    end
  end
end
