class Quota < ActiveRecord::Base
  self.primary_key = "username"

  def self.storage_mb(domain: nil, user: nil)
    if domain
      Quota.where("username LIKE ?", "%@#{domain.domain}").sum(:bytes) / 1.megabyte
    elsif user
      (Quota.find_by(username: user.address)&.bytes || 0) / 1.megabyte
    end
  end

  def self.message_count(domain: nil, user: nil)
    if domain
      Quota.where("username LIKE ?", "%@#{domain.domain}").sum(:messages)
    elsif user
      Quota.find_by(username: user.address)&.messages
    end
  end

end