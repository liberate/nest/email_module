require 'acts_as_list'
require 'random_code'
require 'set'
require 'paper_trail'

module Email
end

module EmailModule
  class Engine < ::Rails::Engine
    isolate_namespace EmailModule

    #
    # make db:migrate automatically pull in engine migrations:
    #
    initializer :append_migrations do |app|
      app.config.paths["db/migrate"] << config.paths["db/migrate"].first
    end
  end

  def self.name
    "Email"
  end
end
